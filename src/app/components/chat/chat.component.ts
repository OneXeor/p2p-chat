import { Component, OnInit, NgZone, ViewChild, ElementRef } from '@angular/core';
import * as Peer from 'peerjs';

interface ChatMessage {
  user: string;
  message: string;
  timestamp: string;
}

@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.scss']
})
export class ChatComponent implements OnInit {
  @ViewChild('textbox', {read: ElementRef}) textbox: ElementRef;

  public usersArray: Array<string> = [];
  public friendId: string;
  public chat: Array<ChatMessage> = [];
  public name: string;

  public peer: any;
  public connection: any;

  constructor(private zone: NgZone) { }

  ngOnInit() {
    this.peer = new Peer({key: 'lwjd5qra8257b9'});

    this.peer.on('connection', (conn) => {
      conn.on('data', (userMessage) => {
        this.zone.run(() => {
          if ((typeof(userMessage) === 'string') && !this.usersArray.some(user => user === userMessage)) {
            this.connect(conn.peer);
            this.usersArray.push(userMessage);
          } else if (typeof(userMessage) === 'object') {
            this.chat.unshift(userMessage);
          }
        });
      });
    });
  }

  public connect(peer?): void {
    this.connection = this.peer.connect(this.friendId ? this.friendId : peer);
    this.connection.on('open', () => {
      this.connection.send(this.name);
    });
  }

  public sendMessage(message): void {
    if (message !== '') {
      const currentTime = (new Date()).toTimeString().substring(0, 8);
      const userMessage: ChatMessage = {
        user: this.name,
        message: message,
        timestamp: currentTime
      };
      this.chat.unshift(userMessage);
      this.connection.send(userMessage);
      this.textbox.nativeElement.value = '';
    }
  }

  public getName(name): void {
    this.name = name;
    this.usersArray.push(name);
  }
}
